#!/bin/bash

yum -y update

yum install -y vim-enhanced
yum install -y git
yum install -y nodejs

# vim environment
cd $HOME
git clone git@gitlab.com:kevin.um/dev-config.git
cp ~/dev-config/.vimrc ~/.vimrc
rm -rf ~/dev-config

# obtain mean files
cd $HOME
git clone https://gitlab.com/kevin.um/web-dev-mean.git

cd $HOME/web-dev-mean
npm init --save
npm install express
npm install mongoose
npm install body-parser

#install mongodb
echo "[mongodb-org-3.4]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.4/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc" > /etc/yum.repos.d/mongodb-org.repo

yum -y install mongodb-org

# start mongodb
systemctl start mongod
